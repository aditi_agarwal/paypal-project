# README
# Python - Django - Paypal - Express Checkout Integration

This is a simple django application which integrates PayPal Express checkout on the sandbox environment using Python.
There are not many clear reosurces that provide information about the same. 

It also has a simple shopping cart/ basket integrated to handle user payments. The project exclusively handles anoymous user by using sessions. 
Each sessionID is stored as a cookie for the user and helps retain the basket and chekout details on next visit. This ensures user does not have to add them again.

In the test environment, it also provides UI for creating, editing and deleting products. Please remove these before using in production environment. 

Dependencies:

1. Python 2.7+
2. Django 1.6+
3. Django Middleware settings turned on
4. Django JSON Serializer

Integration with PayPal Express Checkout (API):

1. SetExpressCheckout()
2. GetExpressCheckoutDetails()
3. DoExpressCheckoutPayment()

Please report any issues or comments in issues page of this repository. 