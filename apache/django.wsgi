import djcelery
djcelery.setup_loader()

##################################################

import os
import sys

path = '/home/ubuntu/hqlocal/hq_local'
if path not in sys.path:
        sys.path.append(path)

# sys.path.append('/home/ubuntu/hqlocal/db')
sys.path.append('/home/ubuntu/hqlocal/')

os.environ['DJANGO_SETTINGS_MODULE'] = 'hq_local.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

