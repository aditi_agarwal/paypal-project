import djcelery
djcelery.setup_loader()

#######################################

import os
import sys

path = '/hqlocal/hq_local'
if path not in sys.path:
    sys.path.append(path)

sys.path.append('/hqlocal/db')
sys.path.append('/hqlocal/')

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
