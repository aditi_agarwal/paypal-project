"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from db.models import Shop, Product

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

    @classmethod
    def test_total_quantity_sold(self):
    	shop = Shop.objects.get(shop_id=1)
    	product = Product.objects.get(barcode='00000008')
    	print shop.totalQuantitySold(product)
    	#{u'product__sum':8.0}