# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals
from django.utils.datastructures import SortedDict
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Sum
import sys
from django.utils import formats
#from pytz import timezone
from hq_local.settings import *

class Product(models.Model):
    barcode = models.CharField(max_length=8L, primary_key=True) # Field name made lowercase.
    name = models.CharField(max_length=256L) # Field name made lowercase.
    category = models.CharField(max_length=32L) # Field name made lowercase.
    manufacturer = models.CharField(max_length=32L) # Field name made lowercase.
    cost_price = models.DecimalField(decimal_places=2, max_digits=12) # Field name made lowercase.
    bundle_unit = models.IntegerField() # Field name made lowercase.  
    minimum_stock = models.IntegerField() # Field name made lowercase.
    is_valid = models.BooleanField(default = True)
    def __unicode__(self):
        return u'%s %s' % (self.barcode, self.name)
    def as_json(self):
        # we use sorted dict to preserve order in which key/value pair is inserted
        # this is so that it won't mess up the json when we try to render it client-side
        dict = SortedDict()
        dict['barcode'] =  self.barcode
        dict['name'] = self.name
        dict['category'] = self.category
        dict['manufacturer'] = self.manufacturer
        dict['cost_price'] = "%0.2f" % self.cost_price
        dict['Quantity'] = '<input type="number" value = "1" name="' + self.barcode +'Q'+ '" id="' + self.barcode +'Q'+'" />'
        dict['AddToBasket'] = '<input type="checkbox" name="' + self.barcode  + '" id="' + self.barcode + '" />'
        return dict
    def as_defjson(self):
        dict = SortedDict()
        dict['barcode'] =  self.barcode
        dict['name'] = self.name
        dict['category'] = self.category
        dict['manufacturer'] = self.manufacturer
        dict['cost_price'] = "%0.2f" % self.cost_price
        dict['Quantity'] = '1'
        return dict
    class Meta:
        db_table = 'product'