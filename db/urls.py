from django.conf.urls import patterns, url

from db import views
from views import product, checkpaypal, Basket, transactionSummary, page

urlpatterns = patterns('',
    url(r'^$', page.home, name='home'),

    url(r'^product.json', product.getJson, name='jsonProduct'),
    url(r'^productdef.json', product.getDefaultJson, name='jsonDefaultProduct'),
    url(r'^product/Basket.json', Basket.getJson, name='jsonBasket'),
    url(r'^product$', product.index, name='indexProduct'),
    url(r'^product/new$', product.new, name='newProduct'),
    url(r'^product/create$', product.create, name='createProduct'),
    url(r'^product/(?P<prod_barcode>\w+)/edit$', product.edit, name='editProduct'),
    url(r'^product/(?P<prod_barcode>\w+)/update$', product.update, name='updateProduct'),
    url(r'^product/(?P<prod_barcode>\w+)/destroy$', product.destroy, name='destroyProduct'),
    url(r'^product/AddToBasket$', Basket.AddToBasket, name='AddToBasket'),
    url(r'^product/ClearBasket$', Basket.clearBasket, name='clearBasket'),

    url(r'^Basket$', Basket.index, name='Basket'),
    url(r'^Basket.json', Basket.getJson, name='jsonBasket'),

    url(r'^DefaultBasket$', product.defindex, name='DefaultBasket'),

    url(r'^product/(?P<prod_barcode>\w+)$', product.show, name='showProduct'),
    url(r'^product/get/(?P<prod_barcode>\w+)$', product.get, name='getProduct'),
    
    url(r'^transactionSummary.json', transactionSummary.getJson, name='jsonTransactionSummary'),
    url(r'^trans$', transactionSummary.ExpressCheckout, name='testSale'),
    url(r'^transactionSummary/CompleteOrder$',transactionSummary.CompleteOrder, name='CompleteOrder'),
    url(r'^transactionSummary/success$',transactionSummary.success, name='success'),
    url(r'^transactionSummary/failure$',transactionSummary.failure, name='failure'),

    url(r'^defaulttrans$', transactionSummary.defaultExpressCheckout, name='defaultSale'),
    url(r'^transactionSummary/defaultCompleteOrder$',transactionSummary.defaultCompleteOrder, name='defaultCompleteOrder'),
    url(r'^transactionSummary/defaultsuccess$',transactionSummary.defaultSuccess, name='defaultsuccess'),
    url(r'^transactionSummary/defaultfailure$',transactionSummary.defaultFailure, name='defaultfailure'),
)
