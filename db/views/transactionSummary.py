from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse
from django.db import transaction, connection
from django.db.utils import DatabaseError
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import SortedDict

from hq_local.settings import PARENT_DIR, TIME_ZONE, MAGIC_STRING
from datetime import datetime, timedelta
from db.models import *
import json
import re
import hashlib
import urlparse
import unittest
import warnings
import unittest
import warnings

from Testpaypal import *

p = PayPal()
def ExpressCheckout(request):
    product_list = request.session['list']
    if product_list:
        #token = p.SetExpressCheckout('500.00', 'http://ec2-54-68-18-226.us-west-2.compute.amazonaws.com/transactionSummary/success', 'http://ec2-54-68-18-226.us-west-2.compute.amazonaws.com/transactionSummary/failure', product_list);    
        token = p.SetExpressCheckout( 'http://127.0.0.1:8000/transactionSummary/success', 'http://127.0.0.1:8000/transactionSummary/failure', product_list);
        s ='https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' + token + "&useraction=commit";
        return HttpResponseRedirect(s);
    else:
        total = '0.0'
        return render(request, 'product/AddToBasket.html', {'total': total})

def defaultExpressCheckout(request):
    products = Product.objects.all().filter(is_valid=True)
    product_list = []
    for prod in products:
        dict = SortedDict()
        dict['barcode'] =  prod.barcode
        dict['name'] = prod.name
        dict['category'] = prod.category
        dict['manufacturer'] =prod.manufacturer
        dict['cost_price'] = prod.cost_price
        dict['quantity'] = '1'
        product_list.append(dict)
    if product_list:
        #token = p.SetExpressCheckout('500.00', 'http://ec2-54-68-18-226.us-west-2.compute.amazonaws.com/transactionSummary/success', 'http://ec2-54-68-18-226.us-west-2.compute.amazonaws.com/transactionSummary/failure', product_list);    
        token = p.SetExpressCheckout( 'http://127.0.0.1:8000/transactionSummary/defaultsuccess', 'http://127.0.0.1:8000/transactionSummary/defaultfailure', product_list);
        s ='https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' + token + "&useraction=commit";
        return HttpResponseRedirect(s);
    else:
        total = '0.0'
        return render(request, 'product/AddToBasket.html', {'total': total})

def defaultSuccess(request):
    url = request.get_full_path()
    parsed =  urlparse.parse_qs(urlparse.urlparse(url).query)
    token = parsed['token'] 
    PayerID = parsed['PayerID']
    details = p.GetExpressCheckoutDetails(token[0], True)
    return render(request, 'transactionSummary/successDefault.html', {'details': details})

def defaultFailure(request):
    return render(request, 'transactionSummary/failureDefault.html')

def defaultCompleteOrder(request):
    url = request.POST.get('txtUrl', False)
    parsed =  urlparse.parse_qs(urlparse.urlparse(url).query)
    token = parsed['token'] 
    PayerID = parsed['PayerID']
    details = p.GetExpressCheckoutDetails(token[0], True)
    p.DoExpressCheckoutPayment(token[0], PayerID[0], details['AMT'][0])
    return render(request, 'transactionSummary/CompleteOrderDefault.html', {'details': details})

def success(request):
    url = request.get_full_path()
    parsed =  urlparse.parse_qs(urlparse.urlparse(url).query)
    token = parsed['token'] 
    PayerID = parsed['PayerID']
    details = p.GetExpressCheckoutDetails(token[0], True)
    request.session['order'] = details
    return render(request, 'transactionSummary/success.html', {'details': details})

def failure(request):
    return render(request, 'transactionSummary/failure.html')

def CompleteOrder(request):
    url = request.POST.get('txtUrl', False)
    parsed =  urlparse.parse_qs(urlparse.urlparse(url).query)
    token = parsed['token'] 
    PayerID = parsed['PayerID']
    details = p.GetExpressCheckoutDetails(token[0], True)
    check = p.DoExpressCheckoutPayment(token[0], PayerID[0], details['AMT'][0])
    #request.session.flush()
    if check['ACK'] == 'Success':
        del request.session['list']
        request.session['list'] = ''
        request.session['total']= '0.0'
        return render(request, 'transactionSummary/CompleteOrder.html', {'details': details})
    else:
        return render(request, 'transactionSummary/failure.html')

def index(request):
    return render(request, 'transactionSummary/index.html')

def indexShop(request,id):
    return render(request, 'transactionSummary/indexShop.html', {'shop_id': id})

# HTTP verb: GET
# Path: along the line of /Success.json?page=0&filter[1]=abc&column[2]=0
# Used in AJAX call from tablesorter jquery plugin from index page to sort/filter/go to next page
# return JSON data
def getJson(request):
    # go through each get request variable
    list = request.session['order']
    dict = SortedDict()
    for name in list['FIRSTNAME']:
        dict['FirstName'] = name
    for name in list['LASTNAME']:
        dict['LastName'] = name
    for name in list['EMAIL']:
        dict['Email'] = name
    for name in list['AMT']:
        dict['AMT'] = name
    for name in list['CURRENCYCODE']:
        dict['CurrencyCode'] = name
    for name in list['TAXAMT']:
        dict['TaxAmount'] = name
    for name in list['TIMESTAMP']:
        dict['Time'] = name    
    dim = []
    dim.append(dict)
    paginator = Paginator(dim, 100) #100 entries at a time
    page = request.GET.get('page') 

    try:
        product_on_page = paginator.page(int(page or 0) + 1) # if page is None, just use 0 ( and + 1 because it's zero-based on client side)
    except EmptyPage:
        product_on_page = paginator.page(paginator.num_pages)

    response_data = {}
    response_data['total_rows'] = 1
    response_data['headers'] = ["First Name", "Last Name", "Email", "Total Amount", "Currency Code", "Tax Amount", "Time"]
    # serializers.serialize("json", array of obj) won't give us a json in a structure that we want.
    # so we are writing out own shit
    response_data['rows'] = dim
    return HttpResponse(json.dumps(response_data), content_type="application/json")