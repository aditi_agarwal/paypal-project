from django.core import serializers
from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
import json

from db.models import Product
from db.views import Basket
from Basket import Basket

# HTTP verb: GET
# Path: /product
# Used for: display list of all products
def index(request):
    # all db query done in getJson function called via "/product.json" through AJAX from the client side
    return render(request, 'product/index.html')

def defindex(request):
    # all db query done in getJson function called via "/product.json" through AJAX from the client side
    return render(request, 'product/Default.html')

# HTTP verb: GET
# Path: along the line of /product.json?page=0&filter[1]=abc&column[2]=0
# Used in AJAX call from tablesorter jquery plugin from index page to sort/filter/go to next page
# return JSON data
def getJson(request):
    # url in form of : http://localhost:8000/product.json?page=0&filter[1]=a&filter[2]=b&column[8]=0
    # go through each get request variable
    product_list = Product.objects.all().filter(is_valid=True)
    for key in request.GET: 
        if key == "filter[0]":
            product_list = product_list.filter(barcode__icontains=request.GET.get(key))
        if key == "filter[1]":
            product_list = product_list.filter(name__icontains=request.GET.get(key))
        if key == "filter[2]":
            product_list = product_list.filter(category__icontains=request.GET.get(key))
        if key == "filter[3]":
            product_list = product_list.filter(manufacturer__icontains=request.GET.get(key))
        if key == "filter[4]":
            product_list = product_list.filter(cost_price__icontains=request.GET.get(key))

    
    for key in request.GET:
        value = request.GET.get(key)
        if key == "column[0]":
            product_list = product_list.order_by("barcode") if value == "0" else product_list.order_by("-barcode")
        if key == "column[1]":
            product_list = product_list.order_by("name") if value == "0" else product_list.order_by("-name")
        if key == "column[2]":
            product_list = product_list.order_by("category") if value == "0" else product_list.order_by("-category")
        if key == "column[3]":
            product_list = product_list.order_by("manufacturer") if value == "0" else product_list.order_by("-manufacturer")
        if key == "column[4]":
            product_list = product_list.order_by("cost_price") if value == "0" else product_list.order_by("-cost_price")

    paginator = Paginator(product_list, 100) #100 entries at a time
    page = request.GET.get('page') 

    try:
        product_on_page = paginator.page(int(page or 0) + 1) # if page is None, just use 0 ( and + 1 because it's zero-based on client side)
    except EmptyPage:
        product_on_page = paginator.page(paginator.num_pages)

    response_data = {}
    response_data['total_rows'] = product_list.count()
    response_data['headers'] = ["Barcode", "Name", "Category", "Manufacturer", "Cost Price", "Quantity", "Add to Basket"]
    # serializers.serialize("json", array of obj) won't give us a json in a structure that we want.
    # so we are writing out own shit
    response_data['rows'] = [ob.as_json() for ob in product_on_page]
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def getDefaultJson(request):
    # url in form of : http://localhost:8000/product.json?page=0&filter[1]=a&filter[2]=b&column[8]=0
    # go through each get request variable
    product_list = Product.objects.all().filter(is_valid=True)
    for key in request.GET: 
        if key == "filter[0]":
            product_list = product_list.filter(barcode__icontains=request.GET.get(key))
        if key == "filter[1]":
            product_list = product_list.filter(name__icontains=request.GET.get(key))
        if key == "filter[2]":
            product_list = product_list.filter(category__icontains=request.GET.get(key))
        if key == "filter[3]":
            product_list = product_list.filter(manufacturer__icontains=request.GET.get(key))
        if key == "filter[4]":
            product_list = product_list.filter(cost_price__icontains=request.GET.get(key))

    
    for key in request.GET:
        value = request.GET.get(key)
        if key == "column[0]":
            product_list = product_list.order_by("barcode") if value == "0" else product_list.order_by("-barcode")
        if key == "column[1]":
            product_list = product_list.order_by("name") if value == "0" else product_list.order_by("-name")
        if key == "column[2]":
            product_list = product_list.order_by("category") if value == "0" else product_list.order_by("-category")
        if key == "column[3]":
            product_list = product_list.order_by("manufacturer") if value == "0" else product_list.order_by("-manufacturer")
        if key == "column[4]":
            product_list = product_list.order_by("cost_price") if value == "0" else product_list.order_by("-cost_price")

    paginator = Paginator(product_list, 100) #100 entries at a time
    page = request.GET.get('page') 

    try:
        product_on_page = paginator.page(int(page or 0) + 1) # if page is None, just use 0 ( and + 1 because it's zero-based on client side)
    except EmptyPage:
        product_on_page = paginator.page(paginator.num_pages)

    response_data = {}
    response_data['total_rows'] = product_list.count()
    response_data['headers'] = ["Barcode", "Name", "Category", "Manufacturer", "Cost Price", "Quantity"]
    # serializers.serialize("json", array of obj) won't give us a json in a structure that we want.
    # so we are writing out own shit
    response_data['rows'] = [ob.as_defjson() for ob in product_on_page]
    return HttpResponse(json.dumps(response_data), content_type="application/json")
# HTTP verb: GET
# Path: /product/new
# Used for: return an HTML form for creating a new product
def new(request):
    return render(request, 'product/new.html')

# HTTP verb: POST
# Path: /product/create
# Used for: create a new product
def create(request):
    try:
        productBarcode = request.POST['productBarcode']
        productName = request.POST['productName']
        productCategory = request.POST['productCategory']
        productManufacturer = request.POST['productManufacturer']
        productCostPrice = request.POST['productCostPrice']
        productMinimumStock = request.POST['productMinimumStock']
        if len(Product.objects.all().filter(barcode = productBarcode)) > 0:
            product = Product.objects.get(barcode = productBarcode)
            if(product.is_valid):
                raise Exception("Duplicate Barcode") 
        if(int(productMinimumStock) < 0):
            raise Exception("Minimum stock cannot be below 0.")
        product = Product(barcode=productBarcode, 
                          name=productName,
                          category = productCategory,
                          manufacturer = productManufacturer,
                          cost_price = productCostPrice,
                          minimum_stock = productMinimumStock,
                          bundle_unit = 0)
        product.save()
        messages.success(request, "Product with barcode %s is successfully created" % str(product.barcode))
        return HttpResponseRedirect(reverse('db:showProduct', args=(productBarcode,)))
    except Exception as e:
        messages.error(request, str(e))
        return render(request, 'product/new.html')


# HTTP verb: GET
# Path: /product/:id
# Used for: display a specific product
def show(request, prod_barcode):
    product = get_object_or_404(Product, barcode=prod_barcode)
    return render(request, 'product/show.html', {'product': product})

# HTTP verb: GET
# Path: /product/:id/edit
# Used for: return an HTML form for return an HTML form for editing a product
def edit(request, prod_barcode):
    product = get_object_or_404(Product, barcode=prod_barcode)
    return render(request, 'product/edit.html', {'product': product})

# HTTP verb: POST
# Path: /product/:id/update
# Used for: update a specific product
# Updating of primary key giving weird error. Can't update any primary key for now.
def update(request, prod_barcode): 
    try:
        product = get_object_or_404(Product, barcode=prod_barcode)
        # product.code = request.POST['productCode'] 
        product.name = request.POST['productName']
        product.category = request.POST['productCategory']
        product.manufacturer = request.POST['productManufacturer']
        product.cost_price = request.POST['productCostPrice']
        product.minimum_stock = request.POST['productMinimumStock']
        if(int(product.minimum_stock) < 0 ):
            raise Exception("Minimum Stock must be greater than or equal to 0.")
        product.save()
        return HttpResponseRedirect(reverse('db:showProduct', args=(prod_barcode,)))
    except Exception as e:
        messages.error(request, str(e))
        return render(request, 'product/edit.html', {
            'product': product
        })

# HTTP verb: ideally should be DELETE, but is not GET
# Path: /product/new
# Used for: return an HTML form for creating a new product
def destroy(request, prod_barcode):
    product = get_object_or_404(Product, barcode=prod_barcode)
    product.is_valid = False
    product.save()
    updateProductShopDetail(product) #if the product becomes invalid, the product should be removed from all the shops
    return HttpResponseRedirect(reverse('db:indexProduct'))
    
def get(request, prod_barcode):
    product = get_object_or_404(Product, barcode = prod_barcode)
    list = [product]
    data = serializers.serialize('xml', list)
    return HttpResponse(str(data))