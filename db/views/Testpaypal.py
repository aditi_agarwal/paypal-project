from django.core import serializers
from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
import json
from hq_local.settings import USER, PWD, SIGNATURE, VERSION
from db.models import Product
import unittest
import warnings
import unittest
import warnings

import urllib, md5, datetime
from cgi import parse_qs

# This class is desgined to call the Paypal API. Not used any PayPal packages to achieve this. 
# PWD, SIGNATURE  and USER is stored safely in the setting.py file to prevent access to everyone other than the admin and root
# Implemented on Sandvox enviroment. Self- declared all API functions for PayPal from the online documentation available.
# There is no direct support available for python and ExpressCheckout

class PayPal:
    """ #PayPal utility class"""
    signature_values = {}
    API_ENDPOINT = ""
    PAYPAL_URL = ""
    
    def __init__(self):
        ## Sandbox values
        self.signature_values = {
        'USER' : USER, # Edit this to your API user name
        'PWD' : PWD, # Edit this to your API password
        'SIGNATURE' : SIGNATURE, # edit this to your API signature
        'VERSION' : VERSION,
        }
        self.API_ENDPOINT = 'https://api-3t.sandbox.paypal.com/nvp' # Sandbox URL, not production
        self.PAYPAL_URL = 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token='
        self.signature = urllib.urlencode(self.signature_values) + "&"

    # API METHODS
    def SetExpressCheckout(self, return_url, cancel_url, product_list, **kwargs):
        params = {
            'METHOD' : "SetExpressCheckout",
            'NOSHIPPING' : 1,
            'PAYMENTACTION' : 'Sale',
            'RETURNURL' : return_url,
            'CANCELURL' : cancel_url,
            'ALLOWNOTE' :1,
            'PAYMENTREQUEST_0_PAYMENTACTION':'Sale',
            'PAYMENTREQUEST_0_CURRENCYCODE':'USD',
            'PAGESTYLE' : 'CentreOne',
        }
        m = 0;
        total = 0;
        for i in product_list:
            key = 'L_PAYMENTREQUEST_0_NAME' + str(m)
            params[key] = i['name']
            key = 'L_PAYMENTREQUEST_0_QTY' + str(m)
            params[key] = i['quantity']
            key = 'L_PAYMENTREQUEST_0_AMT' +str(m)
            params[key] = i['cost_price']
            total += (float(i['cost_price']) * int(i['quantity'])) #change by quantity
            key = 'L_PAYMENTREQUEST_0_DESC' + str(m)
            params[key] = i['manufacturer']
            key = 'L_PAYMENTREQUEST_0_NUMBER' + str(m)
            params[key] = i['barcode']
            m = m + 1
        params['PAYMENTREQUEST_0_AMT'] = total
        params.update(kwargs)
        params_string = self.signature + urllib.urlencode(params)
        response = urllib.urlopen(self.API_ENDPOINT, params_string).read()
        response_dict = parse_qs(response)
        response_token = response_dict['TOKEN'][0]
        return response_token
    
    def GetExpressCheckoutDetails(self, token, return_all = False):
        params = {
            'METHOD' : "GetExpressCheckoutDetails",
            'TOKEN' : token,
        }
        params_string = self.signature + urllib.urlencode(params)
        response = urllib.urlopen(self.API_ENDPOINT, params_string).read()
        response_dict = parse_qs(response)
        if return_all:
            return response_dict
        try:
            response_token = response_dict['TOKEN'][0]
        except KeyError:
            response_token = response_dict
        return response_token
    
    def DoExpressCheckoutPayment(self, token, payer_id, amt):
        params = {
            'METHOD' : "DoExpressCheckoutPayment",
            'TOKEN' : token,
            'AMT' : amt,
            'PAYERID' : payer_id,
            'PAYMENTACTION' : 'Sale',
        }
        params_string = self.signature + urllib.urlencode(params)
        response = urllib.urlopen(self.API_ENDPOINT, params_string).read()
        response_tokens = {}
        for token in response.split('&'):
            response_tokens[token.split("=")[0]] = token.split("=")[1]
        for key in response_tokens.keys():
                response_tokens[key] = urllib.unquote(response_tokens[key])
        print response_tokens
        return response_tokens
        
    def GetTransactionDetails(self, tx_id):
        params = {
            'METHOD' : "GetTransactionDetails", 
            'TRANSACTIONID' : tx_id,
        }
        params_string = self.signature + urllib.urlencode(params)
        response = urllib.urlopen(self.API_ENDPOINT, params_string).read()
        response_tokens = {}
        for token in response.split('&'):
            response_tokens[token.split("=")[0]] = token.split("=")[1]
        for key in response_tokens.keys():
                response_tokens[key] = urllib.unquote(response_tokens[key])
        return response_tokens    