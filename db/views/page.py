from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http import Http404
from hq_local.settings import *

def home(request):
    return render(request, 'home.html')
    
def management(request):
    return render(request, 'management.html')