import hashlib

def hashPassword(password):
	return hashlib.sha1(password).hexdigest()
	
def checkHash(password, hashPassword):
	return hashPassword(password) == hashPassword