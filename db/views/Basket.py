from django.core import serializers
from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.utils.datastructures import SortedDict
import json
from json import dumps, loads, JSONEncoder, JSONDecoder
import pickle
from db.models import Product
import unittest
import warnings
import unittest
import warnings

import urllib, md5, datetime
from cgi import parse_qs
#overide default json encoder to support non-primitive data-types
class PythonObjectEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (list, dict, str, unicode, int, float, bool, type(None))):
            return JSONEncoder.default(self, obj)
        return {'_python_object': pickle.dumps(obj)}

def as_python_object(dct):
    if '_python_object' in dct:
        return pickle.loads(str(dct['_python_object']))
    return dct

#declare products to be added to basket. It cosists of quantity of each product as well
class ProductforBasket:
    barcode = ''
    name = ''
    category = ''
    manufacturer = ''
    cost_price = None 
    quantity = None
    def __init__(self, prod_barcode, quantity, name, category, manufacturer, cost_price):
        self.barcode = prod_barcode
        self.quantity = quantity
        self.name  = name
        self.category = category
        self.manufacturer =  manufacturer
        self.cost_price = cost_price

    def as_json(self):
        # we use sorted dict to preserve order in which key/value pair is inserted
        # this is so that it won't mess up the json when we try to render it client-side
        dict = SortedDict()
        dict['barcode'] =  self.barcode
        dict['name'] = self.name
        dict['category'] = self.category
        dict['manufacturer'] = self.manufacturer
        dict['cost_price'] = "%0.2f" % self.cost_price
        dict['quantity'] = self.quantity
        return dict

#declare shopping basket for every new user using sessions
class Basket:
    productList = None
    def __init__(self):
        self.productList = []

    # API METHODS
    def AddToBasket(self, prod_barcode, quantity, name, category, manufacturer, cost_price):
        P = ProductforBasket(prod_barcode, quantity, name, category, manufacturer, cost_price)
        self.productList.append(P)
        
    def getProductList(self):
         return self.productList

    def RemoveFromBasket(self, prod_barcode):
        for i in productList:
            if( i.barcode == prod_barcode):
                productList.remove(i)

# HTTP verb: GET
# Path: /Basket
# Used for: display the user's active basker
def index(request):
    data = {}
    total = '0.0'
    try:
        total = request.session['total']
    except:
        request.session['total'] = '0.0'
        total = 0.0
    try:
        data = request.session['list']
    except:
        total = request.session['total']
    request.session['list'] = data
    request.session['total'] = total
    return render(request, 'product/AddToBasket.html', {'total': total})

#function called on adding products to basket. Adds the session variable as well.
def AddToBasket(request):
    B = Basket()
    product_l = []
    previous = []
    prevT = '0.0'
    try:
        previous = request.session['list']
    except:
        request.session['list'] = ''
    try:
        prevT = request.session['total']
    except:
        request.session['total'] = '0.0'
    total = 0
    request.session.flush()
    product_list = Product.objects.all().filter(is_valid=True)
    for product in product_list:
        temp = str(product.barcode)
        quant = temp+'Q'
        if temp in request.POST:
            quantity = request.POST[quant]
            B.AddToBasket(product.barcode, quantity, product.name, product.category, product.manufacturer, product.cost_price) 
    productList = B.getProductList()
    if productList is not None:
        for product in productList:
            total += product.cost_price * int(product.quantity)
    response_data = {}
    response_data["rows"] = [ob.as_json() for ob in productList]
    request.session['list'] = ''
    if request.session['list'] is not None:
        for product_list in previous:
            dict = SortedDict()
            dict['barcode'] =  product_list['barcode']
            dict['name'] = product_list['name']
            dict['category'] = product_list['category']
            dict['manufacturer'] =product_list['manufacturer']
            dict['cost_price'] = product_list['cost_price']
            dict['quantity'] = product_list['quantity']
            product_l.append(dict)
        for ob in productList:
            dict = ob.as_json()
            product_l.append(dict)
        nowT = float(prevT) + float(total)
        request.session['list'] = product_l
        request.session['total'] = str(nowT)
    else:
        request.session['list'] = response_data["rows"]
        request.session['total'] = str(total)
    total = request.session['total']
    return render(request, 'product/AddToBasket.html', {'total': total, 'data' : response_data})

# HTTP verb: GET
# Path: /clearBasket
# Used for: clear the contents of the basket
def clearBasket(request):
    request.session.flush()
    request.session['list'] = ''
    request.session['total'] = '0.0'
    total = request.session['total']
    return render(request, 'product/AddToBasket.html', {'total': total})

# HTTP verb: GET
# Path: along the line of /Basket.json?page=0&filter[1]=abc&column[2]=0
# Used in AJAX call from tablesorter jquery plugin from index page to sort/filter/go to next page
# return JSON data
def getJson(request):
    # url in form of : http://localhost:8000/product.json?page=0&filter[1]=a&filter[2]=b&column[8]=0
    # go through each get request variable
    product_l = []
    product = request.session['list']
    for product_list in product:
        dict = SortedDict()
        dict['barcode'] =  product_list['barcode']
        dict['name'] = product_list['name']
        dict['category'] = product_list['category']
        dict['manufacturer'] =product_list['manufacturer']
        dict['cost_price'] = product_list['cost_price']
        dict['quantity'] =product_list['quantity']
        product_l.append(dict)

    for key in request.GET: 
        if key == "filter[0]":
            product_list = product_list.filter(barcode__icontains=request.GET.get(key))
        if key == "filter[1]":
            product_list = product_list.filter(name__icontains=request.GET.get(key))
        if key == "filter[2]":
            product_list = product_list.filter(category__icontains=request.GET.get(key))
        if key == "filter[3]":
            product_list = product_list.filter(manufacturer__icontains=request.GET.get(key))
        if key == "filter[4]":
            product_list = product_list.filter(cost_price__icontains=request.GET.get(key))
        if key == "filter[5]":
            product_list = product_list.filter(quantity__icontains=request.GET.get(key))
    
    for key in request.GET:
        value = request.GET.get(key)
        if key == "column[0]":
            product_list = product_list.order_by("barcode") if value == "0" else product_list.order_by("-barcode")
        if key == "column[1]":
            product_list = product_list.order_by("name") if value == "0" else product_list.order_by("-name")
        if key == "column[2]":
            product_list = product_list.order_by("category") if value == "0" else product_list.order_by("-category")
        if key == "column[3]":
            product_list = product_list.order_by("manufacturer") if value == "0" else product_list.order_by("-manufacturer")
        if key == "column[4]":
            product_list = product_list.order_by("cost_price") if value == "0" else product_list.order_by("-cost_price")
        if key == "column[5]":
            product_list = product_list.order_by("quantity") if value == "0" else product_list.order_by("-quantity")

    paginator = Paginator(product_l, 100) #100 entries at a time
    page = request.GET.get('page') 

    try:
        product_on_page = paginator.page(int(page or 0) + 1) # if page is None, just use 0 ( and + 1 because it's zero-based on client side)
    except EmptyPage:
        product_on_page = paginator.page(paginator.num_pages)

    response_data = {}
    if len(product_l) is not None:
        length = len(product_l)
    else:
        length = 0;
    response_data["total_rows"] = length
    response_data["headers"] = ["Barcode", "Name", "Category", "Manufacturer", "Cost Price", "Quantity"]
    # serializers.serialize("json", array of obj) won't give us a json in a structure that we want.
    response_data["rows"] = product_l
    # print response_data
    return HttpResponse(json.dumps(response_data), content_type="application/json")
